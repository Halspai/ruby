RGB-SELAVY & RGB-LHOOQ
-----------------------

These designs utilize a modified Gyro Baffle made with a diameter of 6.9mm and inteded for with 5.56/.223 Caliber ammunition. 
The RGB-SELAVY (216mm/8.5in) uses two of the gyro-baffles placed in sequence, while the RGB-LHOOQ (164mm/~6.5in) uses a single gyro-baffle. 

The silencer mounts to a standard A2 Birdcage mount, and uses a Worm-Drive Hose clamp such as this one (https://www.mcmaster.com/5415K17) to secure it.
Original idea and model based are from the Freebird Muzzle Adapter by pealerjoe (https://odysee.com/@pealerjoe:a)

Print with the front face of the silencer facing the build plate.

I reccomend using PLA+ (or better), running your temp between 210-215 (or as the material dictates), movement speed at 50mm/s or lower, and no supports.



You will need in total:

1x Print of Either the RGB-LHOOQ or the RGB-SELAVY

1x Print of the FMA-WNS (Freebird Mounting System-Why Not Sneeze)

1x Worm Drive Hose Clamp Adapter for between 1 1/16th" and 2"

1x 5.56/.223 Firearm with an A2 Birdcage Flash Hider


To assemble, simply put the main body of the silencer around the A2 Birdcage. Then lower the FMA-WNS until it fits into place, and finally secure the hose clamp around the A2 Cage.

You should be ready to rock and roll